% class of an agent moving through obstacles
% contains the algorithm for following multiple obstacles
% along with what controller is used to follow the cavf

classdef Agent < handle
    properties
        P;      % position at time t
        phi;    % heading at time t
        V;      % velocity
        phi_des;
        V_des;
        P_hist; % time histories; updated to time t
        V_hist;
        phi_hist;
        % controller gains
        K = 3;
    end

    methods
        function obj = Agent(P,phi_des,V_des)
            obj.P = P;
            obj.phi = phi_des;
            obj.phi_hist = phi_des;
            obj.phi_des = phi_des;
            obj.V_des = V_des;
            obj.V = V_des*[cos(phi_des);sin(phi_des)];
            obj.P_hist = [P];
            obj.V_hist = obj.V;
        end

        % agent controller, outputs velocities (Xdot) given a cavf (h) and current state (velocities)
        function Xdot = controller(obj,h,X)
            P = X(1:2); 
            phi = X(3);
            phi_ref = atan2(h(2),h(1));
            % simple proportional control?
            K = obj.K;
            phi_dot = K*(phi_ref-phi);
            P_dot = obj.V_des * [cos(phi);sin(phi)];
            Xdot = [P_dot;phi_dot];
            %keyboard
            return
            %Xdot = h;
        end

        function [] = update(obj,X,t_new)
            P_new = X(1:2); phi_new = X(3);
            obj.P_hist(:,end+1) = P_new;
            obj.phi = phi_new;
            obj.phi_hist(:,end+1) = phi_new;
            obj.P = P_new;
            obj.V = obj.V_des*[cos(phi_new);sin(phi_new)];
            obj.V_hist(:,end+1) = obj.V;
        end

        function [] = reset(obj)
            obj.P = obj.P_hist(:,1);
            obj.P_hist = obj.P;
            obj.V_hist = obj.V_des*[cos(obj.phi_des);sin(obj.phi_des)];
        end

        function h = plot_hist(obj,idx)
            if(idx > length(obj.P_hist))
                warning('histories are not updated?')
                return
            end
            h = scatter(obj.P_hist(1,idx),obj.P_hist(2,idx),50,'filled');
        end
    end
end
