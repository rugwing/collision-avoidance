clear;
obstacle_case = 'uncertellipse'
animate = true
multiplot = false

switch obstacle_case

    case 'sell'
        a=2;b=2;n=4;
        sell = Shape.superellipse(a,b,n);
        %Po = [5;0];
        Po = [-1;0];
        Vo = [0;0];
        %Vo = [-1;0];
        theta = -pi/4;
        d_i = 6; a_i = 2;
        obs_1 = ObstacleSuperElliptical(sell,Po,Vo,theta,d_i,a_i);
        %agent = Agent([-10;0],0,1);
        %env = Environment(0,agent,obs);
        %tf = 18;

        a=1; b=5;
        ell = Shape.ellipse(a,b);
        Po = [2;0]; 
        Vo = [0;0];
        %theta = 0;
        theta = pi/2;
        %theta = pi/4;
        d_i = 10;
        a_i = 3;
        %obs_1 = ObstacleElliptical(ell,Po,Vo,theta,d_i,a_i)
        obs_2 = ObstacleElliptical(ell,Po+[0;-a*6],Vo,theta+pi/6,d_i,a_i)
        obs_3 = ObstacleElliptical(ell,Po+[0;a*3*2],Vo,theta-pi/6,d_i,a_i)
        obs = [obs_1,obs_2,obs_3];
        agent = Agent([-10;0],0,1);
        env = Environment(0,agent,obs);
        tf = 25;

    case 'expanding'
        delta = 6; % for plotting
        w_exp = .35;
        mag_exp = 2;
        a = @(t) 4+mag_exp*sin(w_exp*t);
        da = @(t) mag_exp*w_exp*cos(w_exp*t);
        b = @(t) 2.5+mag_exp*sin(w_exp*t);
        db = @(t) mag_exp*w_exp*cos(w_exp*t);
        w = 0;
        theta = @(t) w*t;
        omega = @(t) w;
        ell1 = Shape.ellipseMoving(a,b,da,db,theta,omega);
        Po = [6;b(0)+.5];
        Vo = [-.75;0];
        theta = 0;
        d_i = 10;
        a_i = 1;
        obs1 = ObstacleElliptical(ell1,Po,Vo,theta,d_i,a_i);
        a = @(t) 4+mag_exp-sin(w_exp*t);
        da = @(t) -mag_exp*w_exp*cos(w_exp*t);
        b = @(t) 2.5-mag_exp*sin(w_exp*t);
        db = @(t) -mag_exp*w_exp*cos(w_exp*t);
        ell2 = Shape.ellipseMoving(a,b,da,db,@(t)0,omega);
        obs2 = ObstacleElliptical(ell2,[Po(1);-Po(2)],Vo,theta,d_i,a_i);
        %obs2 = ObstacleElliptical(ell2,-Po,-Vo,theta,d_i,a_i);

        obs = [obs1,obs2];
        agent = Agent([-10;0],0,1);
        V_obs = w_exp*mag_exp+norm(Vo)
        env = Environment(0,agent,obs);
        tf = 18;

    case 'rotating'
        delta = 8; % for plotting
        a = @(t) 5;
        da = @(t) 0;
        b = @(t) 1;
        db = @(t) 0;
        w = 1/max(b(0),a(0));
        theta = @(t) w*t;
        omega = @(t) w;
        ell = Shape.ellipseMoving(a,b,da,db,theta,omega);
        Po = [0;0];
        Vo = [0;0];
        theta = 0;
        d_i = 10;
        a_i = 1;
        obs = ObstacleElliptical(ell,Po,Vo,theta,d_i,a_i);
        agent = Agent([-10;0],0,1);
        env = Environment(0,agent,obs);
        tf = 25;

    case 'gap'
        a=1; b=10;
        ell = Shape.ellipse(a,b);
        Po = [2;b+1]; 
        Vo = [0;0];
        theta = 0;
        %theta = pi/2;
        %theta = pi/4;
        d_i = 10;
        a_i = 3;
        obs_1 = ObstacleElliptical(ell,Po,Vo,theta,d_i,a_i)
        obs_2 = ObstacleElliptical(ell,[Po(1),-Po(2)]',Vo,theta,d_i,a_i)
        obs = [obs_1,obs_2];
        agent = Agent([-10;0],0,1);
        env = Environment(0,agent,obs);
        tf = 25;

    case 'multiellipse'
        a=1; b=5;
        ell = Shape.ellipse(a,b);
        Po = [2;0]; 
        Vo = [0;0];
        %theta = 0;
        theta = pi/2;
        %theta = pi/4;
        d_i = 10;
        a_i = 3;
        obs_1 = ObstacleElliptical(ell,Po,Vo,theta,d_i,a_i)
        obs_2 = ObstacleElliptical(ell,Po+[0;-a*6],Vo,theta+pi/6,d_i,a_i)
        obs_3 = ObstacleElliptical(ell,Po+[0;a*3*2],Vo,theta-pi/6,d_i,a_i)
        obs = [obs_1,obs_2,obs_3];
        agent = Agent([-10;0],0,1);
        env = Environment(0,agent,obs);
        tf = 25;

    case 'ellipse'
        a=1; b=5;
        ell = Shape.ellipse(a,b);
        Po = [2;0]; 
        Vo = [0;0];
        theta = 0;
        %theta = pi/2;
        %theta = pi/4;
        d_i = 10;
        a_i = 2;
        obs_ell = ObstacleElliptical(ell,Po,Vo,theta,d_i,a_i)
        agent = Agent([-10;0],0,1);
        env = Environment(0,agent,obs_ell);
        tf = 25;

    case 'uncertellipse'
        delta = 6;
        adot = .15; a0 = 2;
        bdot = .35; b0 = 1;
        a = @(t) a0+adot*t;
        da = @(t) adot;
        b = @(t) b0+bdot*t;
        db = @(t) bdot;

        a2 = @(t) a0+adot/2*t;
        da2 = @(t) adot/2;
        b2 = @(t) b0+bdot/2*t;
        db2 = @(t) bdot/2;

        theta = @(t) 0;
        omega = @(t) 0;
        ell1 = Shape.ellipseMoving(a,b,da,db,theta,omega);
        ell2 = Shape.ellipseMoving(a2,b2,da2,db2,theta,omega);

        Po = [6;0]; Po2_off = [7;7];
        Vo = [-.75;0];
        theta = 0;
        d_i = 10;
        a_i = 1;
        obs1 = ObstacleElliptical(ell1,Po,Vo,theta,d_i,a_i);
        obs2 = ObstacleElliptical(ell2,Po+Po2_off,Vo,theta,d_i,a_i);
        obs = [obs1,obs2];

        agent = Agent([-10;0],0,1);
        env = Environment(0,agent,obs);
        tf = 24;

    end

    env.iterate(tf);

    if animate
        env.animate_hist(true,'uncertellipse_anim.gif');
    end

    %figure(2); clf;

    X = agent.P_hist;
    t = env.t_hist;

    if multiplot
        num = floor(t(end)/delta); 

        for n=1:num
            figure(n+1); clf;
            t_start = delta*(n-1)
            t_end = delta*n
            c_start = max(find(t<=t_start));
            c_end = max(find(t<t_end));
            env.plot(t_start);
            plot(X(1,c_start:c_end),X(2,c_start:c_end),'LineWidth',3,'Color',[0,.7,.2]);
            %for o=obs
            %    obs_next1 = o.plot(t_start+delta/2);
            %    obs_next1.LineStyle='--';
            %    %obs_next2 = o.plot(t_end);
            %    %obs_next2.LineStyle='--';
            %end
            title(['t = [' num2str(t_start) ' , ' num2str(t_end) ']']);
        end
    end

    if strcmp(obstacle_case,'uncertellipse') && multiplot
        for n=1:num
            figure(n+1);
            plot([Po(1),Po(1)+tf*Vo(1)],[Po(2)+b(0),Po(2)+tf*Vo(2)+b(tf)],'--','LineWidth',2,'Color',[.6,.3,.1])
            plot([Po(1),Po(1)+tf*Vo(1)],[Po(2)-b(0),Po(2)+tf*Vo(2)-b(tf)],'--','LineWidth',2,'Color',[.6,.3,.1])

            plot([Po(1)+Po2_off(1),Po(1)+Po2_off(1)+tf*Vo(1)],[Po(2)+Po2_off(2)+b2(0),Po(2)+Po2_off(2)+tf*Vo(2)+b2(tf)],'--','LineWidth',2,'Color',[.6,.3,.1])
            plot([Po(1)+Po2_off(1),Po(1)+Po2_off(1)+tf*Vo(1)],[Po(2)+Po2_off(2)-b2(0),Po(2)+Po2_off(2)+tf*Vo(2)-b2(tf)],'--','LineWidth',2,'Color',[.6,.3,.1])
        end

    end
